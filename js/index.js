// var voucherList = [{"id": 271,"voucherID": "VCH001RNA892RTN","memberID": "MEMTT142336DlHic","email": "facejun@yahoo.com","name": "20% OFF YOUR BILL from TUESDAY to SUNDAY","image": "http:\/\/192.168.1.9\/project\/bistro\/assets\/images\/vouchers\/S1G0201608231455385i56.png","startDate": "2016-09-06","endDate": "2017-09-06","type": "discounts","cardType": "BFF","cardVersion": "BFF2016","frequencyType": "day","frequencyStart": "1","frequencyEnd": "6","parameterString": "DISCPRC","parameterValue": "20","action": "COPY","status": "active","dateAdded": "2016-09-06 14:10:36","dateModified": "2016-09-06 14:10:36","count": 1,"weekDay": 1,"description": "20% OFF YOUR BILL from TUESDAY to SUNDAY","terms": "Card holder is entitled to 20% OFF on  purchase of F &amp; B in participating Bistro Group restaurants from Tuesdays to Sundays\n\n30% OFF your bill every Monday\nComplimentary single serving of regular coffee or hot tea with entreepurchase. Exclusive for card holder only.\n\nBistro Frequent Foodie Discounts can be combined with the Bistro Frequent Foodie vouchers \nBistro Frequent Foodie Discounts are not to be combined with other discounts and promotions, senior citizen\/PWD discounts, party packages\nNot valid for purchase of gift certificates\nNot valid on Mother&rsquo;s day, Father&rsquo;s day and Valentine&rsquo;s Day. \n\nNo card, no discount\nDiscount can be availed right after purchase up to a total discount value of P1,500 on initial card use\nBFF  vouchers can be used on the next visit \nThe Bistro Group reserves the right of final interpretation of all terms and conditions of use, which are subject to change without prior notice"}, {"id": 266,"voucherID": "VCH002RNB814RXL","memberID": "MEMTT142336DlHic","email": "facejun@yahoo.com","name": "P250.00 OFF E-VOUCHERS","image": "http:\/\/192.168.1.9\/project\/bistro\/assets\/images\/vouchers\/iEU920160823145608vELC.png","startDate": "2016-09-06","endDate": "2017-09-06","type": "benefits","cardType": "BFF","cardVersion": "BFF2016","frequencyType": "day","frequencyStart": "0","frequencyEnd": "6","parameterString": "DISCAMT","parameterValue": "250","action": "MOVE","status": "active","dateAdded": "2016-09-06 14:10:36","dateModified": "2016-09-06 14:10:36","count": 4,"weekDay": 1,"description": "P250.00 OFF E-VOUCHERS","terms": "The P250 off voucher can be used on the next visit after The BFF Card purchase\n\nEnjoy the P250 off voucher with a minimum purchase of P1,000. One voucher per visit only\n\nThe P250 off voucher can be combined with the Bistro Frequent Foodie discounts\n\nNot valid on Mother&rsquo;s day, Father&rsquo;s day and Valentine&rsquo;s Day\n\nVoucher is non-transferable. Voucher cannot be converted to cash\n\nCannot be combined with other promotions and discounts except the Bistro Premiere Card discount privileges\n\nThe Bistro Group reserves the right to final interpretation of all terms and conditions of which are subject to change without prior notice\n\n The voucher is valid for one year from purchase date. Same validity with BFF Card\n"}, {"id": 273,"voucherID": "VCHSq32054mkr4T6","memberID": "MEMTT142336DlHic","email": "facejun@yahoo.com","name": "Complimentary Regular Coffee or Hot Tea","image": "http:\/\/192.168.1.9\/project\/bistro\/assets\/images\/vouchers\/AFBt201608241540126Iem.png","startDate": "2016-09-06","endDate": "2017-09-06","type": "discounts","cardType": "BFF","cardVersion": "BFF2016","frequencyType": "day","frequencyStart": "0","frequencyEnd": "6","parameterString": "DISCPRC","parameterValue": "100","action": "COPY","status": "active","dateAdded": "2016-09-06 14:10:36","dateModified": "2016-09-06 14:10:36","count": 1,"weekDay": 1,"description": "Complimentary Regular Coffee or Hot Tea","terms": "Get 350 Less on Bulgogi Borthers"}, {"id": 270,"voucherID": "VCHu2152347yuGMG","memberID": "MEMTT142336DlHic","email": "facejun@yahoo.com","name": "P1000 OFF BIRTHDAY E-VOUCHER","image": "http:\/\/192.168.1.9\/project\/bistro\/assets\/images\/vouchers\/4WgF201608241522471VWH.png","startDate": "2016-09-06","endDate": "2017-09-06","type": "birthday","cardType": "BFF","cardVersion": "BFF2016","frequencyType": "month","frequencyStart": "0","frequencyEnd": "6","parameterString": "DISCAMT","parameterValue": "1000","action": "MOVE","status": "active","dateAdded": "2016-09-06 14:10:36","dateModified": "2016-09-06 14:10:36","count": 0,"weekDay": 1,"description": "P1000 OFF BIRTHDAY E-VOUCHER","terms": "The P1000 off birthday voucher should be claimed on your birth month\n\nGet P1000 off your total bill with a minimum purchase of P3,000\n\n Present the Bistro Frequent Foodie Card and I.D. bearing your birthday to avail of the P1000 off Birthday voucher\nP1000 off birthday voucher can only be used once.\n\nThe P1000 off birthday voucher can be combined with the Bistro Frequent Foodie card discount\n\nVoucher is non-transferable. Voucher cannot be converted to cash\n\nCannot be combined with the P250 off voucher , other promotions and discounts except the BFF Card discount\n\nNot valid on Mother&rsquo;s day, Father&rsquo;s day, and Valentine&rsquo;s day\n\nThe Bistro Group reserves the right to final interpretation of all terms and conditions of which are subject to change without prior notice\n"}, {"id": 274,"voucherID": "VCHUf212926p8hPa","memberID": "MEMTT142336DlHic","email": "facejun@yahoo.com","name": "We Miss You","image": "http:\/\/192.168.1.9\/project\/bistro\/assets\/images\/vouchers\/695V20160824214826ZP5c.png","startDate": "2016-09-06","endDate": "2016-10-06","type": "wemissyou","cardType": "BFF","cardVersion": "BFF2016","frequencyType": "year","frequencyStart": "0","frequencyEnd": "6","parameterString": "DISCAMT","parameterValue": "500","action": "MOVE","status": "active","dateAdded": "2016-09-06 14:19:19","dateModified": "2016-09-06 14:19:19","count": 1,"weekDay": 1,"description": "We Miss You Very Much","terms": "We Miss You Very Much"}];
// var voucherList = [];
var selectedVoucher = [];
var redeemAmount;

$(function(){
	// redeemCallback({"points":"1000","voucher":"Three (3) Php 200 off","transactionID":"7c897ac968b1aba5OW11504170760748"});
	addEventListeners();
	getBadgeCount('pointtable');
	// initializeDOB();
	// redeemCallback({"points":"1000","voucher":"Three (3) Php 200 off","transactionID":"7c897ac968b1aba5OW11504170760748"});
	// scanCallback('earn', '[{"profile": {"qrCard": "2ismeu832m3j3i", "email": "jericho@yahoo.com", "totalPoints": 1000, "dateOfBirth": "10/10/2016", "lastVisited":"Shangrila (Italiannis)", "lastTransaction":"2016-09-16 12:06:48"}}]');
});

function addEventListeners(){
	FastClick.attach(document.body);

	$('#settings').on('click', function(){ nativeInterface.settings(); });
	$("#syncRecord").on('click', function(e){ 
		nativeInterface.syncLocallyStoreData('pointtable'); 
	});

	$('#registerBtn').on('click', function(){
		// TEST
		register();
	});

	$('#earnBtn').on('click', function(){
		nativeInterface.scan("earn");
		// TEST
		// scanCallback('earn', '[{"profile": {"qrCard": "2ismeu832m3j3i", "email": "jericho@yahoo.com", "totalPoints": 1000, "dateOfBirth": "10/10/2016", "lastVisited":"Shangrila (Italiannis)", "lastTransaction":"2016-09-16 12:06:48"}}]');
		// offlineEarnCallback('QHAID09182393');
	});	

	$('#redeemBtn').on('click', function(){
		nativeInterface.scan("redeem");

		// TEST
		// scanCallback('redeem', '[{"profile":[{"memberID": "MEM5F1234154RjMi","fname": "Jahdiel","mname": "","lname": "Mcatangy","email": "jahdiel@appsolutely.ph","mobileNum": "+639566390987","landlineNum": "","dateReg": "Dec 12,2017","qrApp":"2017000000000106","qrCard": "GRUB201708J00061","occupation": "","civilStatus": "","dateOfBirth": "","address1": "","address2": "","city": "","province": "","zip": "","gender": "","image": "","profileStatus": "incomplete","expiration": "2018-12-12 12:09:54","totalPoints": "10030","accumulatedPoints": "10030","lastTransaction":"December 13, 2017","lastVisited": "TRINOMA"}]}]');
	});

	$('#activateAcc').on('click', function(){
		activateAccount();
	});

	$('#privacy').on('click', function(){
		showPrivacyModal();
	});

	$('.modalCancel').on('click', function(){
		$('.modalConfirm.btn').unbind('click');
		$('#modal').removeClass('choice').removeClass('active-modal');
	});

	$('#modal #close').on('click', function(){
		$('.modalConfirm.btn').unbind('click');
		$('#modal').removeClass('choice').removeClass('active-modal');
		$('#modal').removeClass('withInput').removeClass('active-modal');
		$('.modal.policy').removeClass('active-modal');
	});

	$('.logout').on('click', function(){
		$('.modalConfirm.btn').unbind('click');
		confirmLogout();
	});

	$('#vouchers ul').on('click', selectVoucher);
	$('#summary .container #close.btn').on('click', function(){
		$('.active-view').removeClass('active-view');
		$('#home').addClass('active-view');
		nativeInterface.done();
	});
}

function getBadgeCount(type){
	var count = nativeInterface.getEarnedCount("pointtable");
	// var count = 0;
	if (typeof count != "undefined" && count != 0 && count != null && count != "") {
		document.querySelector('#syncRecord').setAttribute('data-rec', count);
		$('#syncRecord').addClass('active-sync');
		$('#syncRecord').show();
	}else{
		document.querySelector('#syncRecord').setAttribute('data-rec', 0);
		$('#syncRecord').prop('display', true);
		$('#syncRecord').removeClass('active-sync');
		$('#syncRecord').hide();
	}
}

function confirmLogout(){
	$('#modal.choice .modalConfirm.btn').off('click');
	$('#modal').addClass('active-modal').addClass('choice');
	$('#modal .header').html('Logout');
	$('#modal .message').html('Are you sure you want to Logout?');

	$('#modal.choice .modalConfirm.btn').on('click', function(){
		logout();
	});
}

function activateAccount(){
	$('.modalConfirm').off('click');
	$('#modal').removeClass('choice');
	$('#modal').addClass('withInput');
	$('#modal').addClass('active-modal');

	$('.modalConfirm').on('click', function(){
		var mobileNum = $('.input_field input').val();

		if ( mobileNum == '' ) {
			$('.input_field input').focus();
			nativeInterface.msgBox('Please enter your mobile number', 'Empty Field!');
		} else if ( !/^\d+$/.test(mobileNum)) {
			$('.input_field input').focus();
			nativeInterface.msgBox('Please enter a valid mobile number', 'Invalid Input!');
		} else {
			mobileNum = '+63' + mobileNum;
			// registrationCallback(mobileNum);
			nativeInterface.resendActivation(mobileNum);
		}
	});
}

function showPrivacyModal(){
	$('.modal.policy').addClass('active-modal');
}

function register(){

	initializeDOB();

	$('#createAccount').off('click');
	$('#scanCard').off('click');

	$('#home').removeClass('active-view');
	$('#register').addClass('active-view');

	if ( $('#mobile').val() == '' ) {
		$('#mobile').val('+63')
	}

	$('#createAccount').on('click', function(){
		validateForm();
	});

	$('#scanCard').on('click', function(){
		nativeInterface.scan("register");
		// TEST
		// scanCallback('register', '012392390291');
	});

	$('#cancel').on('click', function(){
		$('.modalConfirm.btn').unbind('click');
		confirmCancellation();
	});

}

function confirmCancellation(){
	$('#modal').removeClass('withInput');
	$('#modal').addClass('active-modal').addClass('choice');
	$('.header').html('Confirm Cancel');
	$('.message').html('Are you sure you want to cancel your registration?');

	$('#modal.choice .modalConfirm.btn').on('click', function(){
		logout();
	});
}

function validateForm(){
	var card = $('#card').val(),
	fname = $('#fname').val(),
	lname = $('#lname').val(),
	mobile = $('#mobile').val(),
	password = $('#password').val(),
	email = $('#email').val(),
	agent = $('#agent').val(),
	dob_month = $('#month').val(),
	dob_day = $('#day').val(),
	dob_year = $('#year').val();
	numbersOnly = /^\d+$/;

	if ( card == '' ) {
		$('#card').focus();
		nativeInterface.msgBox('Please enter your card number or scan your card. If you have not yet purchased a card, please get one to become a member of Grub Club.', 'Empty Field!');
	} else if ( mobile == '+63' ) {
		$('#mobile').focus();
		nativeInterface.msgBox('Please provide your mobile number', 'Empty Field!');
	} else if ( fname == '' ) {
		$('#fname').focus();
		nativeInterface.msgBox('Please provide your first name', 'Empty Field!');
	} else if ( !/^[a-zA-Z]*$/g.test(fname) ) {
		$('#fname').focus();
		nativeInterface.msgBox('Invalid First name', 'Invalid Input!');
	} else if ( lname == '' ) {
		$('#lname').focus();
		nativeInterface.msgBox('Please provide you last name', 'Empty Field!');
	} else if ( !/^[a-zA-Z]*$/g.test(lname) ) {
		$('#lname').focus();
		nativeInterface.msgBox('Invalid Last name', 'Invalid Input!');
	}else if ( email == '' ) {
		$('#email').focus();
		nativeInterface.msgBox('Please provide your email address', 'Empty Field!');
	} else if ( !validateEmail(email) ) {
		$('#email').focus();
		nativeInterface.msgBox('You have entered an invalid email address. Please try again.', 'Invalid Input!');
	} else if ( password == '' ) {
		$('#password').focus();
		nativeInterface.msgBox('Please provide your password', 'Empty Field!');
	} else if ( !numbersOnly.test(password) ) {
		$('#password').focus();
		nativeInterface.msgBox('Your password should contain 6 digits', 'Invalid Input!');
	} else if ( password.length  > 6 || password.length  < 6 ) {
		$('#password').focus();
		nativeInterface.msgBox('Your password should contain 6 digits', 'Invalid Input!');
	} else if( dob_month == null || dob_day == null || dob_year == null ){
		nativeInterface.msgBox('Please provide your date of birth', 'Empty!');
	} else {
		var data = {
			qrCard: card,
			fname: fname,
			lname: lname,
			mobileNum: mobile,
			email: email,
			password: password,
			dateOfBirth: dob_year +"-"+dob_month+"-"+dob_day,
			serverName: agent
		}
		// console.log(data);
		nativeInterface.register(JSON.stringify(data));
		// TEST 
		// registrationCallback(data);
		
	}
}

function registrationCallback( data ){
	$('#resendCode').off('click');
	$('#verify').off('click');
	$('.active-view').removeClass('active-view');
	$('.modal').removeClass('choice').removeClass('withInput').removeClass('active-modal');
	$('#verification').addClass('active-view');
 
 	$('#verification h2 span').html(data);
	$('#verification h2 span').html(data.mobileNum);

	$(".input_field").keyup(function () {
		if (this.value.length == this.maxLength) {
			$(this).next('.input_field').focus();
		}
	});

	$('#resendCode').on('click', function(){
		nativeInterface.resendActivation(data);
	});

	$('#verify').on('click', function(){
		var code = '';
		$('#code .input_field').each(function(){
			code = code + $(this).val();
		});

		console.log(data);

		if ( code == '' ) {
			nativeInterface.msgBox('Please enter your verification code', 'Empty Field!');
		} else if ( code.length != 6 ) {
			nativeInterface.msgBox('Incorrect verification code', 'Invalid Input!');
		} else if ( !/^\d+$/.test(code) ) {
			nativeInterface.msgBox('Please enter a valid verification code', 'Invalid Input!');
		} else {
			if ( data.hasOwnProperty('mobileNum') ) {
				nativeInterface.verifyAccount( code, data.mobileNum );
			} else {
				nativeInterface.verifyAccount( code, data );
			}

			// nativeInterface.verifyAccount(code, data.mobileNum );
			// $('.active-view').removeClass('active-view');
			// $('#home').addClass('active-view');
			$('#code .input_field').val('');
		}
	});
}

function scanCallback( type, data ){ 
	if ( type == 'register' ) {
		$('#card').val(data);
	} else if ( type == 'earn' ) {
		earn(JSON.parse(data));
	} else if ( type == 'redeem' ) {
		redeem(JSON.parse(data));
	}
}

function offlineEarnCallback( qrCode ){
	$('#confirmEarn').off('click');

	$('.active-view').removeClass('active-view');
	$('#earn').addClass('active-view');

	$('.card_number').html(qrCode);

	$('#earn .container #confirmEarn.btn').on('click', function(){
		var transaction = $('#transID').val(),
		amount = $('#earn .container .earn_details #earnAmtW').val(),
		decimal = $('#earn .container .earn_details #earnAmtD').val(),
		numbersOnly = /^\d+$/;

		if ( transaction == '' ) {
			$('#transaction').focus();
			nativeInterface.msgBox("Please enter the Transaction ID from the customer's receipt.", 'Empty Field!');
		} else if ( amount == '' ) {
			$('#earn .container .input_container #amount').focus();
			nativeInterface.msgBox('Please enter amount.', 'Empty Field!');
		} else if ( !numbersOnly.test(amount) ) {
			$('#earn .container .input_container #earnAmtW').focus();
			nativeInterface.msgBox('Minimum points to redeem: 25', 'Insufficient Credits!');
		} else {
			if ( decimal == '' ) {
				decimal = '00';
				console.log(decimal);
			} 
			var data = {
				contentData: [{
					transID: transaction,
					amount: amount +"."+ decimal
				}]
			};
			console.log(data.contentData);
			confirmEarn(data.contentData);
		}

	});
}

function confirmEarn(data){
	console.log(data);
	$('.modalConfirm').off('click');
	$('#modal').addClass('choice').addClass('active-modal');
	$('.message').html('Are you sure you want to continue with the following earn details?<br><br> <span>Transaction ID</span>' +
		'<div>' + data[0].transID + '</div>' + '<span>Amount Earned</span> <br>' +
		'<div>' + data[0].amount + '</div>'
	);

	$('.modalConfirm').on('click', function(){
		console.log(data);
		$('#modal').removeClass('choice').removeClass('active-modal');
		nativeInterface.earn(JSON.stringify(data));
		// logout();
	});	

}

function redeem( data ){
	console.log(data);
	$('#submit').off('click')
	$('#modal.choice .modalConfirm.btn').off('click');
	$('.active-view').removeClass('active-view');
	$('#redeem').addClass('active-view');
	$('#redeem_points input').val('')

	var profile = data[0].profile[0];
	console.log(profile.memberID);

	$('#redeem.view .redeem_details .points').html(profile.totalPoints + " Points");
	$('#redeem.view .redeem_details .name').html(profile.fname + " " + profile.lname );
	$('#redeem.view .redeem_details .email').html(profile.email);
	$('#redeem.view .redeem_details .card_number').html(profile.qrCard);
	$('#redeem.view .redeem_details .dob').html("Date of Birth: " + profile.dateOfBirth);
	$('#redeem.view .redeem_details .card_number').html(profile.qrCard);
	$('#redeem.view .redeem_details .lvs').html("Last Visited Store: " + profile.brandName + ", " + profile.lastVisited);
	$('#redeem.view .redeem_details .lt').html("Last Transaction: " + profile.lastTransaction);
	$('#redeem.view .redeem_details .exp').html("Expiration Date: " + profile.expiration.split(' ')[0]);

	nativeInterface.getVoucherList(profile.qrCard, false); //MemberID
	// vouchersCallback(voucherList);

	$('#submit').on('click', function(){

		redeemAmount = $('#redeem_points input').val(),
		numbersOnly = /^\d+$/;

		if ( redeemAmount == '' && (selectedVoucher == '' || selectedVoucher == undefined) ) {
		} else{
			if ( redeemAmount == '' ) {
				console.log('voucher', selectedVoucher);
				var redeemData = [{
					points: 0,
					vouchers: selectedVoucher
				}];
				// console.log([{"points":0,"vouchers":[{"id":"VCH0v14957NpRHr6","name":"Four (4) Php 100 off "}]}]);

				$('#modal').addClass('choice').addClass('active-modal');
				$('#choice .message').html("Are you sure you want to redeem " + selectedVoucher[0].name + " ?");
				$('#modal.choice .modalConfirm.btn').on('click', function(){
					// redeemCallback(redeemData);
					nativeInterface.redeem(JSON.stringify(redeemData));
				});
			} else if ( selectedVoucher.length == 0 )  {
				if ( !numbersOnly.test(redeemAmount) || redeemAmount == '0' ) {
					$('#redeem_points input').focus();
					nativeInterface.msgBox('Invalid input', 'Oops!');
				} else if ( parseInt(redeemAmount) > parseInt(profile.totalPoints) ) {
					$('#redeem_points input').focus();
					nativeInterface.msgBox('Sorry you do not have enough points to redeem this item', 'Insufficient Credits!');
				} else {
					var redeemData = [{
						points: parseInt(redeemAmount),
						vouchers: []
					}];
					// var redeemData = {points:"100","transactionID":"uBt1513241096654"};
					console.log(redeemAmount);
					$('#modal').addClass('choice').addClass('active-modal');
					$('#choice .message').html("Are you sure you want to redeem " + redeemAmount + " points?");

					$('#modal.choice .modalConfirm.btn').on('click', function(){
						// redeemCallback(redeemData);
						nativeInterface.redeem(JSON.stringify(redeemData));
					});
				}
			} else {
				confirmRedeem(redeemAmount)
			}
		}
	});
}

function redeemCallback(data){
	var summaryData = JSON.parse(data);
	// var summaryData = data //TEST
	// var summaryData = {"voucher":"Four (4) Php 100 off ","transactionID":"r5o1513672466552"};
	// var summaryData = {"points":"100","voucher":"Four (4) Php 100 off ","transactionID":"Z8N1513672722952"};
	// var summaryData = {"points":"100","transactionID":"rwD1513673183273"};
	console.log(data);
	$('.active-modal').removeClass('active-modal');
	$('#modal').removeClass('choice');
	$('.active-view').removeClass('active-view');
	$('#summary').addClass('active-view');

	if ( summaryData.hasOwnProperty('points') ) {
		if ( !summaryData.voucher ) {
			$('.points').html(summaryData.points);
			$('.summary_voucher').hide();
			$('.summary_points').show();
		}else {
			$('.points').html(summaryData.points);
			$('.voucher').html(summaryData.voucher);
			$('.summary_points').show();
			$('.summary_voucher').show();
		}
	} else if ( summaryData.hasOwnProperty('voucher') ) {
		$('.voucher').html(summaryData.voucher);
		$('.summary_points').hide();
		$('.summary_voucher').show();
	} 

	$('.transactionID').html(summaryData.transactionID);

}

function vouchersCallback(data){
	var vouchers = JSON.parse(data);
	// var vouchers = data //TEST

	console.log(vouchers);
	if ( vouchers.length > 0 ) {
		var content = '';

		for (var i = 0; i < vouchers.length; i++) {
			content += '<li class="voucher_details" data-count= "'+vouchers[i].count+'" data-name="'+vouchers[i].name+'" data-id="'+vouchers[i].voucherID+'">'+ vouchers[i].name +'</li>';
		}

		$('#vouchers ul').html(content);
		$('#vouchers').show();
		$('#empty').hide();
	} else {
		console.log('asd');
		$('#redeem .container #vouchers').hide();
		$('#empty').show();
	}

	
}

function confirmRedeem(points){
	$('.modalConfirm').off('click');
	$('#modal').addClass('choice').addClass('active-modal');
	$('.message').html('Are you sure you want to continue with the following redeem details?<br><br> <span>Amount</span>' +
		'<div>' + redeemAmount + '</div>' + '<span>Voucher</span> <br>' +
		'<div>' + selectedVoucher[0].name + '</div>'
	);

	$('.modalConfirm').on('click', function(){
		var redeemData = [{
			points: parseInt(redeemAmount),
			vouchers: selectedVoucher
		}];
		console.log(redeemData);
		console.log('both');
		// redeemCallback(redeemData);
		// redeemCallback({"points":"1000","voucher":"Three (3) Php 200 off","transactionID":"7c897ac968b1aba5OW11504170760748"});
		nativeInterface.redeem(JSON.stringify(redeemData));
	});	
}



function selectVoucher(e){
	selectedVoucher = [];
	$('.modalConfirm').off('click');
	if ( e.target != e.currentTarget ) {
		
		var name = e.target.getAttribute('data-name');
		var id = e.target.getAttribute('data-id');

		if ( $(e.target).hasClass('selected') ) {
			$(e.target).removeClass('selected');
			selectedVoucher = [];
		} else {
			$('.selected').removeClass('selected');
			$(e.target).addClass('selected');
			var voucherDetails = {
				id: id,
				name: name
			}
			selectedVoucher.push(voucherDetails);
			console.log(voucherDetails);
		}
	}
}

function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}

function initializeDOB(){
	setMonths();
	setDays();
	setYears();
	$("#month, #year").off('change');
	$("#month, #year").on('change', updateDays);	
}

function setMonths(){
	var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	var options = "<option selected disabled value='month'>Month</option>";
	for (var i = 1; i <= months.length; i++) { 
		if( i <= 9 ){
			options += "<option value='0"+i+"'>"+months[i-1]+"</option>"; 
		}else{
			options += "<option value='"+i+"'>"+months[i-1]+"</option>"; 
		}
	}
	$("#month").html(options);
}

function setDays(){
	var options = "<option selected disabled value='day'>Day</option>";
	for (var i = 1; i <= 31; i++) { 
		if( i <= 9 ){
			options += "<option value='0"+i+"'>"+i+"</option>"; 
		}else{
			options += "<option value='"+i+"'>"+i+"</option>"; 
		}
	}
	$("#day").html(options);
}

function setYears(){
	var options = "<option selected disabled value='year'>Year</option>";
	for (var i = new Date().getFullYear(); i >= 1940; i--) { options += "<option value='"+i+"'>"+i+"</option>"; }
	$("#year").html(options);
}

function updateDays(e){
	var selMonth = ($("#month").val() != '') ? $("#month").val() : 1;
	var selYear = ($("#year").val() != '') ? $("#year").val() : new Date().getFullYear();
	var days = new Date(selYear, selMonth, 0).getDate();
	var selDay = ($("#day").val() == '' || parseInt($("#day").val()) > days) ? '' : $("#day").val();
	var options = "<option selected disabled value='day'>Day</option>";
	for (var i = 1; i <= days; i++) {
	 if( i <= 9 ){
			options += "<option value='0"+i+"'>"+i+"</option>"; 
		}else{
			options += "<option value='"+i+"'>"+i+"</option>"; 
		}
	}
	$("#day").html(options).val(selDay);
}



function logout(){
	getBadgeCount('pointtable');
	getBadgeCount('registertable');
	$("input").val('').blur();

	$(".active-view").removeClass('active-view');
	$("#home.view").addClass('active-view');

	$(".active-modal").removeClass('active-modal');
	$('#modal').removeClass('choice');
	$('#modal').removeClass('withInput');

}